﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationManager : MonoBehaviour
{

  public GameObject target;

  public List<GameObject> agents;

  public float radius;
  public float maxSpeed;
  public float maxAcceleration;
  public float slowRadius;
  public float targetRadius;
  public Vector3 targetVelocity;
  public float targetSpeed;
  public float distance;
  public Vector3 velocity = Vector3.zero;

  private GameObject[] formationSlots;

  // Use this for initialization
  void Start()
  {
    formationSlots = new GameObject[agents.Count];
    float x = this.transform.position.x + this.radius;
    float z = this.transform.position.z;
    float angle = 360f / agents.Count;
    for (int i = 0; i < agents.Count; i++)
    {
      formationSlots[i] = new GameObject("Slot_" + i);
      GameObject go = agents[i];
      SteerSeekColl agent = go.GetComponent<SteerSeekColl>();
      agent._target = formationSlots[i].transform;
      Vector3 newPosition = GetCircularPosition(x, z, this.transform.position.x, this.transform.position.z, angle * (i + 1));
      newPosition.y = this.transform.position.y;
      formationSlots[i].transform.position = newPosition;
    }
  }

  // Update is called once per frame
  void Update()
  {
    velocity = velocity + GetSteering()._linear * Time.deltaTime; // Vt = Vo +a.t

    if (velocity.magnitude > maxSpeed)
    {
      velocity = velocity.normalized * maxSpeed;

    }

    this.transform.position = transform.position + velocity * Time.deltaTime;
    this.transform.eulerAngles = SteeringData.getNewOrientation(transform.eulerAngles, velocity);

    for (int i = 0; i < agents.Count; i++)
    {
      formationSlots[i].transform.position = formationSlots[i].transform.position + velocity * Time.deltaTime;
      formationSlots[i].transform.eulerAngles = SteeringData.getNewOrientation(formationSlots[i].transform.eulerAngles, velocity);

    }
  }

  Vector3 GetCircularPosition(float x, float z, float a, float b, float angle)
  {

    float radian = (angle * Mathf.PI) / 180f;
    float x_aksen = (x - a) * Mathf.Cos(radian) - ((z - b) * Mathf.Sin(radian)) + a;
    float z_aksen = (x - a) * Mathf.Sin(radian) - ((z - b) * Mathf.Cos(radian)) + b;
    Vector3 v = new Vector3(x_aksen, 0, z_aksen);
    return v;
  }

  public SteeringData GetSteering()
  {
    SteeringData steeringOut = new SteeringData();
    steeringOut._linear = target.transform.position - transform.position; //#direction

    distance = steeringOut._linear.magnitude;
    if (distance > slowRadius)
    {
      targetSpeed = maxSpeed;
    }
    else if (distance <= targetRadius)
    {
      targetSpeed = 0;
    }
    else
    {
      targetSpeed = maxSpeed * distance / slowRadius;
    }

    targetVelocity = steeringOut._linear.normalized * targetSpeed;

    steeringOut._linear = (targetVelocity - velocity);

    if (steeringOut._linear.magnitude > maxAcceleration)
    {
    	steeringOut._linear = steeringOut._linear.normalized;
    	steeringOut._linear *= maxAcceleration;
    }
    return steeringOut;
  }
}
